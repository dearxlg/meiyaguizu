<?php
namespace app\user\controller;
use think\Db;
class Shop extends Common{
    protected $uid;
    public function initialize()
    {
        parent::initialize();
        $this->uid=session('user.id');
    }	
	
	
   public function buycar(){
	   if(empty($_SESSION['think']['user']['id'])){
		   
		 return 0;  //没登录
		
		   
	   }
	  $shop_id=input('shopid');
	  $info['shop_id']=$shop_id;
	  $info['userid']=$_SESSION['think']['user']['id'];
	  $info['time']=time();
	  //判断商品是否存在购物车内
	  $userinfo=db('shop_car')->where('shop_id='.$shop_id)->where('userid='.$_SESSION['think']['user']['id'])->find();
	  if(!empty($userinfo['id'])){
		  
			return 10; //商品已在购物车内
	  }
	  
	  if(db('shop_car')->insert($info)!==false){
            return 1; //添加成功
        }else{
            return 2; //添加失败
        }
	   
	   
	   
   }
   public function listbuy(){
				$key=$_SESSION['think']['user']['id'];
				
				
				$lists=db('shop_car')->alias('u')
                ->join(config('database.prefix').'shop ul','u.shop_id = ul.id','left')
                ->field('ul.id as bid,u.*,ul.title,ul.thumb,ul.jiage,ul.kucun')
                ->where('u.userid','=',$key)
                ->order('u.id desc')->select();
				
		$this->assign('lists',$lists);
		return view();
	   
	   
   }
   public function dellshop(){
	   $id=input('id');
	   //判断是不是自己的产品
	   $dingdan= db('shop_car')->where('id='.$id)->find();
	   if($dingdan['userid']==$_SESSION['think']['user']['id']){
		   
		     db('shop_car')->where('id='.$id)->delete(); 
			 return 1;
		   
	   }else{
		   
		   return '2';
		   
	   }
	   
	
	   
	   
	   
   }
   public function jisuan(){
	   $id=input('id');
	   $num=input('num');
	   
	  
	   $dingdan= db('shop')->where('id='.$id)->find();
	   $dingdan=$dingdan['jiage'];
	   if($num<=1){
		   
		   $num=1;
	   }
	   $jiage=$dingdan*$num;
	   return $jiage;
	   
	  
	   
	
	   
	   
	   
   }
   
    public function buy(){
		
		
			$id=input('id');
			$num=input('num');
			
			 if(empty($id)){
		   
					return '没选择产品';
					
			}
			$userid=$_SESSION['think']['user']['id'];
			//获取产品价格
			$cp=db('shop')->where('id='.$id)->find();
			
			//获取用户信息
			$user=db('users')->where('id='.$userid)->find();
			
			//检查库存是否充足
			
			
			if(empty($num)){
				$num=1;
				
			}
			
			if($cp['kucun']<$num){
				
				return '库存不足'; 
				//exit;
				
			}
			//看看订单是否已经生成
			
			$dans = db('shop_dingdan')->where('userid='.$userid)->where('shop_id='.$id)->where('num='.$num)->where('pay_ok=0')->find();
			
		
			if(empty($dans['id'])){
					
				
					
						//生成订单
					
					$yCode = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J');
					$orderSn = $yCode[intval(date('Y')) - 2011] . strtoupper(dechex(date('m'))) . date('d') . substr(time(), -5) . substr(microtime(), 2, 5) . sprintf('%02d', rand(0, 99));//订单号
					$dan['shop_id']=$id;
					$dan['userid']=$userid;
					$dan['danhao']=$orderSn;
					$dan['pay_ok']=0;
					$dan['num']=$num;
					$dan['time']=time();
					
					
				
					$dingdan=$cp['jiage'];
					   if($num<=1){
						   
						   $num=1;
					   }
					   
					 $jiage=$cp['jiage']*$num;
					 
					 
					 
					 $dan['xiaofei']=$jiage;
					 $buy_next=db('shop_dingdan')->insertGetId($dan); // 添加订单
				//	 return $dingdan;
					 
					$dan['zongjia']=$jiage;
			}else{
					
					
					$dan=db('shop_dingdan')->where('id='.$dans['id'])->find();
					
					   if($num<=1){
						   
						   $num=1;
					   }
					   
					 
					$dan['zongjia']= $cp['jiage']*$dan['num'];
					$buy_next=$dans['id'];
				
			}
			//获取允许支付类型
			$payopen=db('money_config')->where('open=1')->select();
			
			
			$this->assign('buyid',$buy_next);//输出订单ID
			$this->assign('ding_info',$dan);//输出订单信息
			
			$this->assign('pay',$payopen);
			$this->assign('user',$user);
			//输出当前用户信息以及是否允许信息
			
			
	   
		return view();
	   
	   
   }

   public function buyok(){
	   
			
			$id=input('dingid');
			
			//根据订单获取产品价格
			$type=input('paytype'); 
			$shopid=db('shop_dingdan')->where('id='.$id)->find();
			$num=$shopid['num'];
			$userid=$_SESSION['think']['user']['id'];
			
			//判断是否已经完成交易
			
			if($shopid['pay_ok']==1){
					$zt=4;
					$zt2='订单已经支付过了';
						
			}else{
						//获取产品信息
					
						$cp=db('shop')->where('id='.$shopid['shop_id'])->find();
					
						$jine=$cp['jiage']*$num;
						
						
						//  jifen($type,$userid,$num，$source,$jiajian)
						//  type          1为积分    2为金钱    3 为本金  4 为其他
						//  num           数值
						//  $jiajian     1 为 加       2为减
						//  $source       1商城消费  2 会员提现  3会员转账  4 管理变动  5 手动充值  6 微信充值
						//  返回结果     1、执行成功   2 余额不足
							$jianjia=jifen($type,$userid,$jine,1,2,'');
							
							if($jianjia==1){
								//修改订单状态
									$info['pay_ok']=1;
									db('shop_dingdan')->where('id='.$id)->update($info);
								//减少库存
							db('shop')->where('id='.$shopid['shop_id'])->setDec('kucun',$num);
								//删除购物车
								
									db('shop_car')->where('shop_id='.$shopid['shop_id'])->where('userid='.$userid)->delete(); 
									$zt=1;
									$zt2='恭喜！支付已经成功';
								
							}
							if($jianjia==2){
								$zt=2;
								$zt2='抱歉，余额不足';
								
							}
						}
				$this->assign('zhuangtai2',$zt2);
				$this->assign('zhuangtai',$zt);
					
			
			
			

			
			
			
	   
		return view();
	   
	   
   }
}