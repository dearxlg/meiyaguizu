<?php
namespace app\user\controller;
use think\Input;
class Index extends Common{
    public function initialize(){
        parent::initialize();

    }
    public function index(){
        $this->assign('title','会员中心');
		//获取所有的用户信息
		$user=db('users')->where('id='.$_SESSION['think']['user']['id'])->find();
        $lname=db('user_level')->where('level',$user['level'])->value('level_name');
        $is_wang=db('users')->where(array('uid'=>$_SESSION['think']['user']['id'],'is_b'=>0))->count();
		$this->assign('user',$user);
		$this->assign('is_wang',$is_wang);
		$this->assign('lname',$lname);
        return $this->fetch();
    }
}