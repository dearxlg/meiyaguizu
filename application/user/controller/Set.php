<?php
namespace app\user\controller;
use think\Db;
use think\facade\Request;
use Qrcode\Qrcode;
class Set extends Common{
    protected $uid;
    public function initialize()
    {
        parent::initialize();
        $this->uid=session('user.id');
    }	
	
	
    public function index(){
        if(request()->isPost()){
            $data = input('post.');
            $user = db('users');
            $oldEmail = $user->where('id',$this->uid)->value('email');
            if(Db::name('users')->where([['email','=',$data['email']],['id','neq',$this->uid]])->find()){
                return ['status'=>0,'msg'=>'该邮箱已被注册！'];
            }

            if($oldEmail != $data['email']){
                $data['email_validated'] = 0;
            }
            if (Db::name('users')->where('id',$this->uid)->update($data)!==false) {
                $result['msg'] = '编辑资料成功!';
                $result['status'] = 1;
            } else {
                $result['msg'] = '编辑资料失败!';
                $result['status'] = 0;
            }
            return $result;
        }else{
            $province = Db::name('Region')->where ('pid',1)->select ();
            $this->assign('province',$province);
            $city = Db::name('Region')->where ( 'pid',$this->userInfo['province'])->select ();
            $this->assign('city',$city);
            $district = Db::name('Region')->where ('pid',$this->userInfo['city'])->select ();
            $this->assign('district',$district);
            $this->assign('title','基本设置');
            return $this->fetch();
        }
    }
    public function getRegion(){
        $list=Db::name("region")->where('pid',input("pid"))->select();
        return $list;
    }
    public function avatar(){
        $data = input('post.');
        db('users')->where(['id'=>$this->uid])->update($data);
        return true;
    }
    /**
     * 修改密码
     * @param $old_password  旧密码
     * @param $new_password  新密码
     * @param $confirm_password 确认新 密码
     */
    public function repass(){
        $old_password  = input('post.nowpass');
        $new_password = input('post.pass');
        $confirm_password = input('post.repass');

        if(strlen($new_password) < 6)
            return array('status'=>0,'msg'=>'密码不能低于6位字符');
        if($new_password != $confirm_password)
            return array('status'=>0,'msg'=>'两次密码输入不一致');
        //验证原密码
        if(($this->userInfo['password'] != '' && md5($old_password) != $this->userInfo['password']))
            return array('status'=>0,'msg'=>'密码验证失败');
        if(db('users')->where("id", $this->uid)->update(array('password'=>md5($new_password)))!==false){

            return array('status'=>1,'msg'=>'修改成功','action'=>url('index'));
        }else{
            return array('status'=>0,'msg'=>'修改失败');
        }
    }
    public function unbind(){
        db('oauth')->where("uid",$this->uid)->delete();
        session('user.qq','0');
        return array('status'=>1,'msg'=>'QQ已解绑','action'=>url('index'));
    }
	public function tixian(){
		//获取所有的用户信息
		$user=db('users')->where('id='.$_SESSION['think']['user']['id'])->find();
		if(Request::isAjax()){
		    $data=input('post.');
            if(empty($data['bank_name'])){
                return array('code'=>0,'msg'=>'请先完善银行信息，再申请');
            }

		    if($data['num']<0||empty($data['num'])){
                return array('code'=>0,'msg'=>'请输入正确的金额');
            }
            if($data['num']>$user['money_jifen']){
                return array('code'=>0,'msg'=>'当前余额不足，请勿操作！');
            }
            $min=db('system')->where('id=1')->value('min_tixian');
            if($data['num']<$min){
                return array('code'=>0,'msg'=>'抱歉！最小提现额为：'.$min);
            }
            $type=$data['titype'];
            $num=$data['num'];
            //扣除手续费
            $tixianfei=db('system')->where('id=1')->value('tixian_fei');
            $baifenbi=$tixianfei/100;
            $shifa_qian=$num-$num*$baifenbi ;

            //  jifen($type,$userid,$num，$source,$jiajian)
            //  type          1为积分    2为 金钱    3 为本金  4 为其他
            //  $source       1商城消费  2 会员提现  3会员转账  4 管理变动  5 手动充值  6 微信充值
            //  num           数值
            //  $jiajian     1 为 加       2为减
            //  返回结果     1、执行成功   2 余额不足

            $jieguo=jifen($type,$user['id'],$num,2,2,'');
            if($jieguo==1){
                //提现成功，写进提现表
                $tt['userid']=$user['id'];
                $tt['type']=$type;
                $tt['tixian_time']=time();
                $tt['num']=$num;
                $tt['shifa_qian']=$shifa_qian;
                db('tixian')->insert($tt);
                return array('code'=>1,'msg'=>'恭喜提现申请成功');
            }
        }
		$this->assign('user',$user);
		//获取所有允许提现的列表
		$tixianlist=db('money_config')->where('tixian=1')->select();
		$this->assign('tixian',$tixianlist);
		
		
		
		return view();
	}
/*	public function tixianok(){
		$num=input('num');
		$userid=$_SESSION['think']['user']['id'];
		$type=input('titype');
		//判断是否满足提现要求
		$min=db('system')->where('id=1')->value('min_tixian');
		if($num<$min){
			
			$zt=2;
			$zt2='抱歉！最小提现额为：'.$min;
				$this->assign('zhuangtai2',$zt2);
				$this->assign('zhuangtai',$zt);
				return view();
			return;
			
		}
		
	
		//扣除手续费
		$tixianfei=db('system')->where('id=1')->value('tixian_fei');
		$baifenbi=$tixianfei/100;
		$shifa_qian=$num-$num*$baifenbi ; 
		
		//  jifen($type,$userid,$num，$source,$jiajian)
		//  type          1为积分    2为 金钱    3 为本金  4 为其他
		//  $source       1商城消费  2 会员提现  3会员转账  4 管理变动  5 手动充值  6 微信充值
		//  num           数值
		//  $jiajian     1 为 加       2为减
		//  返回结果     1、执行成功   2 余额不足
		
		$jieguo=jifen($type,$userid,$num,2,2,'');
		
		if($jieguo==1){
			
			//提现成功，写进提现表
			
			$tt['userid']=$userid;
			$tt['type']=$type;
			$tt['tixian_time']=time();
			$tt['num']=$num;
			$tt['shifa_qian']=$shifa_qian;
			db('tixian')->insert($tt);
			
			$zt=1;
			$zt2='恭喜提现申请成功';
			
		}
		if($jieguo==2){
			
			$zt=2;
			$zt2='抱歉！余额不足。';
		}
		$this->assign('zhuangtai2',$zt2);
		$this->assign('zhuangtai',$zt);
		return view();
	}*/
	public function tixian_record(){
		
		$userid=$_SESSION['think']['user']['id'];
		$lists = db('tixian')->where('userid='.$userid)->order('tixian_time desc')->paginate(20);
		
			 
		$page = $lists->render();
		$this->assign('page', $page);
        $this->assign('lists', $lists);
         $this->fetch();
		 return view();
		
		
	}
	public function dingdan(){
        $userid=$_SESSION['think']['user']['id'];
        $type = input('type');
        $is_ok = input('is_ok');
        if($type==0){
            $lists = db('shop_dingdan')->where(array('userid'=>$userid,'pay_ok'=>$type))->order('time desc')->paginate(20);
            $select=1;
        }
        if(isset($is_ok)){
            $lists = db('shop_dingdan')->where(array('userid'=>$userid,'is_ok'=>$is_ok,'pay_ok'=>1))->order('time desc')->paginate(20);
            if($is_ok==0){
                $select=2;
            }else{
                $select=3;
            }
        }
        if(!empty($lists)){
            $page = $lists->render();
            $this->assign('page', $page);
            $this->fetch();
        }
        $this->assign('select', $select);
        $this->assign('lists', $lists);
		 return view();
		
	}

	//删除订单
	public  function del_order(){
	    $id=input('id');
        $userid=$_SESSION['think']['user']['id'];
        $order=db('shop_dingdan')->where(array('id'=>$id))->find();
        if($order['userid']==$userid){
            if($order['pay_ok']==0){
                Db('shop_dingdan')->where('id',$id)->delete();
                $this->redirect('/user/');
            }
        }
    }
	public function daili(){
        $levellist=db('user_level')->where('level_id','gt',0)->select();
        $this->assign('levels',$levellist);

        if(Request::isAjax()) {
            $table = db('users');
            $data = input('post.');
            $username = $data['username'];
            $mobile = $data['mobile'];
            $password = $data['password'];
            $topmobi = $data['topmobi'];
            $centermobile = $data['center_id'];
            $shenfenzheng = $data['shenfenzheng'];
            $fathername = $data['fathername'];
            $treeplace = $data['treeplace'];
            $level = $data['level'];
            $info['level']=$level;
            if(!$username || !$password){
                return array('code'=>0,'msg'=>'请填写账号或密码');
            }
            if(empty($username )|| empty($data['password'])){
                return array('code'=>-1,'msg'=>'请输入昵称或密码');
            }
            $info['username']=$username;
            if(empty($shenfenzheng )){
                return array('code'=>-1,'msg'=>'请输入身份证号');
            }
            $info['shenfenzheng']=$shenfenzheng;
            $user = $table->where("mobile","=",$mobile)->whereOr('email','=',$username)->find();
            if($user){
                return array('code'=>0,'msg'=>'账号已存在!');
            }else{
                $info['mobile']=$mobile;
            }
            if(empty($password)){
                return array('code'=>0,'msg'=>'密码不能为空!');
            }else{
                $info['password']=md5($password);
            }
            $alen=strlen($password);
            if($alen<6||$alen>12){
                return array('code'=>-1,'msg'=>'请输入6-12位的密码');
            }
            if(empty($topmobi)){
                return array('code'=>0,'msg'=>'推荐人手机号不能为空！！！');
            }else{
                $reus = db('users')->where(array('mobile'=>$topmobi,'activation'=>1))->find();
                if(empty($reus)){
                    return array('code'=>0,'msg'=>'推荐人不存在或者未激活！');
                }else{
                    $rpath="".$reus['rpath'].$reus['id'].",";
                    $info['up_userid']=$reus['id'];
                    $info['rpath']=$rpath;
                }
            }
            if(empty($fathername)){
                return array('code'=>0,'msg'=>'安置人手机号不能为空！！！');
            }else{
                $father = db('users')->where(array('mobile'=>$fathername,'activation'=>1))->find();
                if(empty($father)){
                    return array('code'=>0,'msg'=>'安置人不存在或者未激活！');
                }else{
                    $is_user=db('users')->where('fatherid',$father['id'])->where('treeplace',$treeplace)->find();
                    if(empty($is_user)){
                        $ppath="".$father['ppath'].$father['id'].",";
                        $info['fatherid']=$father['id'];
                        $info['ppath']=$ppath;
                        $info['plevel']=$father['plevel']+1;
                    }else{
                        return array('code'=>0,'msg'=>'该位置已有会员，请更换位置后注册！');
                    }

                }
            }
            if(empty($centermobile)){
                return array('code'=>0,'msg'=>'服务中心手机号不能为空！！！');
            }else{
                $server = db('users')->where(array('mobile'=>$centermobile,'activation'=>1,'server'=>2))->find();
                if(empty($server)){
                    return array('code'=>0,'msg'=>'服务中心不存在或者未激活！');
                }else{
                    $info['center_id']=$server['id'];
                }
            }
            $info['reg_time'] = time();
            $info['treeplace'] = $treeplace;
            achievement(0,0,1,1,0,0,0,0,0);
            $uid = db('users')->insertGetId($info);
            db('users')->where('id',$uid)->setField('uid',$uid);
            if($treeplace==1){
                db('users')->where('id',$father['id'])->setField('leftid',$uid);
            }else{
                db('users')->where('id',$father['id'])->setField('rightid',$uid);
            }
            return array('code'=>1,'msg'=>'注册成功','url' => url('index/index'));

        }
        return view();
		
	}
//	public function dailireg(){
//		$info['center_id']=input('center_id');
//		$info['password']=md5(input('password'));
//		$info['up_userid']=db('users')->where('mobile='.input('topmobi'))->value('id');
//		$info['reg_time']=time();
//		$info['mobile']=input('mobile');
//		$info['username']=input('username');
//		if(empty($info['mobile'])){
//
//			$zhuangtai=2;
//			$zhuangtai2='抱歉，手机号不能为空';
//			$this->assign('zhuangtai',$zhuangtai);
//			$this->assign('zhuangtai2',$zhuangtai2);
//			 return view('tixianok');
//		}else{
//
//				//查询是否被注册过
//				$uerid=db('users')->where('mobile='.$info['mobile'])->value('id');
//				if(!empty($uerid)){
//					$zhuangtai=2;
//					$zhuangtai2='手机号已经被注册过啦';
//					$this->assign('zhuangtai',$zhuangtai);
//					$this->assign('zhuangtai2',$zhuangtai2);
//					 return view('tixianok');
//
//				}
//
//		}
//
//
//		if(empty(input('password'))){
//
//			$zhuangtai=2;
//			$zhuangtai2='密码不能为空';
//			$this->assign('zhuangtai',$zhuangtai);
//			$this->assign('zhuangtai2',$zhuangtai2);
//			 return view('tixianok');
//		}
//
//		if(empty($info['up_userid'])){
//
//			$zhuangtai=2;
//			$zhuangtai2='推荐人手机号不存在';
//			$this->assign('zhuangtai',$zhuangtai);
//			$this->assign('zhuangtai2',$zhuangtai2);
//
//			 return view('tixianok');
//		}else{
//            $ruser=db('users')->where('id',$info['up_userid'])->find();
//            $rpath="".$ruser['rpath'].$ruser['id'].",";
//            $info['rpath']=$rpath;
//        }
//        if(empty($info['center_id'])){
//            $zhuangtai=2;
//            $zhuangtai2='请输入服务中心手机号';
//            $this->assign('zhuangtai',$zhuangtai);
//            $this->assign('zhuangtai2',$zhuangtai2);
//            return view('tixianok');
//        }else{
//            $serverid=db('users')->where(array('mobile'=>$info['center_id'],'is_lock'=>1,'server'=>2))->value('id');
//            if(empty($serverid)){
//                $zhuangtai=2;
//                $zhuangtai2='该服务中心不存在或者未激活';
//                $this->assign('zhuangtai',$zhuangtai);
//                $this->assign('zhuangtai2',$zhuangtai2);
//                return view('tixianok');
//            }else{
//                $info['center_id']=$serverid;
//            }
//        }
//
//
//			db('users')->insert($info);
//
//		$zhuangtai=1;
//		$zhuangtai2='恭喜，注册成功';
//
//		$this->assign('zhuangtai',$zhuangtai);
//		$this->assign('zhuangtai2',$zhuangtai2);
//		 return view('tixianok');
//
//
//	}
	public function zhuan(){
        //获取当前信息
        $userid=$_SESSION['think']['user']['id'];
        $user=db('users')->where('id='.$userid)->find();
        $this->assign('user', $user);

        //获取允许转账类型
        $qiantype=db('money_config')->where('zhuan=1')->select();
        $this->assign('qiantype',$qiantype);
        if(Request::isAjax()) {
            $data = input('post.');
            if(empty($data['usermobile'])){
                return array('code'=>0,'msg'=>'请输入转账会员手机号');
            }else{
                $buser=db('users')->where(array('mobile'=>$data['usermobile']))->find();
                if(empty($buser)){
                    return array('code'=>0,'msg'=>'该会员不存在');
                }
            }
            if(empty($data['num'])||$data['num']<0){
                return array('code'=>0,'msg'=>'请输入正确的金额');
            }
            if($user['money_jifen']<$data['num']){
                return array('code'=>0,'msg'=>'余额不足，请勿操作');
            }
            $to_userid=db('users')->where('mobile='.$data['usermobile'])->value('id');
                //  jifen($type,$userid,$num，$source,$jiajian,$beizhu)
                //  type          1为积分    2为 金钱    3 为本金  4 为其他
                //  $source       1商城消费  2 会员提现  3会员转账  4 管理变动  5 手动充值  6 微信充值
                //  num           数值
                //  $jiajian     1 为 加       2为减
                //  返回结果     1、执行成功   2 余额不足
                $beizhu='对方手机号：'.$data['usermobile'];
                $zhuan=jifen($data['type'],$userid,$data['num'],3,2,$beizhu);//先把自己的钱扣掉
                if($zhuan==1){
                    //增加给对方
                    //根据手机号查询对方的会员ID
                    $usermobile=db('users')->where('id='.$_SESSION['think']['user']['id'])->value('mobile');
                    $beizhu='对方手机号：'.$usermobile;
                    jifen($data['type'],$to_userid,$data['num'],3,1,$beizhu);//把对应的钱加给别人
                    return array('code'=>1,'msg'=>'转账成功');
                }
        }
		 return view();
	}
//	public function zhuanok(){
//		$userid=$_SESSION['think']['user']['id'];
//		$num=input('num');
//		$type=input('type');
//		$usermobile=input('usermobile');
//		$to_userid=db('users')->where('mobile='.$usermobile)->value('id');
//		$beizhu='对方手机号：'.$usermobile;
//
//
//
//			if(empty($to_userid)){
//				$zhuangtai=2;
//				$zhuangtai2='对方会员不存在';
//
//			}else{
//					//  jifen($type,$userid,$num，$source,$jiajian,$beizhu)
//					//  type          1为积分    2为 金钱    3 为本金  4 为其他
//					//  $source       1商城消费  2 会员提现  3会员转账  4 管理变动  5 手动充值  6 微信充值
//					//  num           数值
//					//  $jiajian     1 为 加       2为减
//					//  返回结果     1、执行成功   2 余额不足
//					$zhuan=jifen($type,$userid,$num,3,2,$beizhu);//先把自己的钱扣掉
//					if($zhuan==1){
//						//增加给对方
//						//根据手机号查询对方的会员ID
//						$usermobile=db('users')->where('id='.$_SESSION['think']['user']['id'])->value('mobile');
//						$beizhu='对方手机号：'.$usermobile;
//						jifen($type,$to_userid,$num,3,1,$beizhu);//把对应的钱加给别人
//						$zhuangtai=1;
//						$zhuangtai2='转账成功';
//
//					}else{
//
//						$zhuangtai=2;
//						$zhuangtai2='余额不足，转账失败';
//
//					}
//
//			}
//		$this->assign('zhuangtai',$zhuangtai);
//		$this->assign('zhuangtai2',$zhuangtai2);
//		 return view('tixianok');
//	}
	public function zhuan_record(){
		$userid=$_SESSION['think']['user']['id'];
		$lists = db('money_log')->where('userid='.$userid)->where('source=3')->order('time desc')->paginate(20);
		
			 
		$page = $lists->render();
		$this->assign('page', $page);
        $this->assign('lists', $lists);
         $this->fetch();
		
		 return view();
		
	}
	public function zhuan_show(){
		//获取转账详情
		$id=input('id');
		$info=db('money_log')->where('id='.$id)->find();
	
		$this->assign('info',$info);
		
		 return view();
		
	}
	public function jihuo(){
		$userid=$_SESSION['think']['user']['id'];
		$lists=db('users')->where(array('center_id'=>$userid,'activation'=>0))->order('reg_time desc')->paginate(20);;
		$page = $lists->render();

		$this->assign('page', $page);
		$this->assign('lists',$lists);
		$usesr=db('users')->where('id='.$userid)->find();
        $level=getulevel($usesr['level']);
		$this->assign('user',$usesr);
		$this->assign('level_name',$level['level_name']);

		 $this->fetch();
		 return view();
		
	}
	public function jihuouser(){
		$userid=$_SESSION['think']['user']['id'];
		$server=getuser($userid);
        $id=input('id');
		//获取会员信息
		$user=db('users')->where('id',$id)->find();
		$level=getulevel($user['level']);
		if($server['money_baodan']<$level['lsk']){
            $result=array('success'=>0,'msg'=>'您的余额不足，请充值后再试');
            echo    json_encode($result);
        }else{
            if($user['activation']==1){
                $result=array('success'=>0,'msg'=>'会员已激活，请勿重复激活');
                echo    json_encode($result);
            }else{
                jifen(5,$userid,$level['lsk'],8,2,$user['mobile']);
                baodanjiang($user['center_id'],1,$user['mobile']);
                db('users')->where('id',$id)->setInc('activation');
                jihuo($id,1);

                $result=array('success'=>1,'msg'=>'激活成功');
                echo    json_encode($result);
            }
        }
	}
    public function jihuo_record(){
        $userid=$_SESSION['think']['user']['id'];
        $lists = db('money_log')->where(array('userid'=>$userid,'source'=>8))->order('time desc')->paginate(20);
        $page = $lists->render();
        $this->assign('page', $page);
        $this->assign('lists', $lists);
        $this->fetch();

        return view();

    }
	public function pay(){
		
		
		 return view();
		
	}
	public function dizhi(){
		//获取当前用户信息
		$userid=$_SESSION['think']['user']['id'];
		$user=db('users')->where('id='.$userid)->find();
		$this->assign('user',$user);
		
		 return view();
		
	}
    public function mybank(){
        $userid=$_SESSION['think']['user']['id'];
        //获取当前用户信息
        $user=db('users')->where('id='.$userid)->find();
        if(Request::isAjax()){
            $data=input('post.');
            if(empty($data['bank_dizhi'])){
                return array('code'=>0,'msg'=>'请输入开户地址!');
            }
            if(empty($data['bank_card'])){
                return array('code'=>0,'msg'=>'请输入开户银行账号!');
            }
            if(empty($data['username'])){
                return array('code'=>0,'msg'=>'请输入开户姓名!');
            }
            if(empty($data['bank_name'])){
                return array('code'=>0,'msg'=>'请输入开户银行!');
            }


            if($data['password']!=$user['password']){
                $alen=strlen($data['password']);
                if($alen<6||$alen>32){
                    return array('code'=>-1,'msg'=>'请输入6-32位的密码');
                }
                $data['password']=md5($data['password']);
            }
            if($data['twopassword']!=$user['twopassword']){

                $alen=strlen($data['twopassword']);
                if($alen<6||$alen>32){
                    return array('code'=>-1,'msg'=>'请输入6-12位的二级密码');
                }
                $data['twopassword']=md5($data['twopassword']);
            }

            db('users')->where(['id'=>$this->uid])->update($data);
            return array('code'=>1,'msg'=>'保存成功！');
        }


        $this->assign('user',$user);

        return view();

    }
	public function mybankok(){
		$info['dizhi']=input('dizhi');
		$info['bank_name']=input('bank_name');
		$info['bank_dizhi']=input('bank_dizhi');
		$info['username']=input('username');
		$info['bank_card']=input('bank_card');
		
		db('users')->where(['id'=>$this->uid])->update($info);
		$zhuangtai=1;
		$zhuangtai2='修改成功';
		
		
		$this->assign('zhuangtai',$zhuangtai);
		$this->assign('zhuangtai2',$zhuangtai2);
		 return view('tixianok');
		
	}
	public function pay_record(){
		
		
		 return view();
		
	}
	public function baodan(){
        if(Request::isAjax()) {
            $data = input('post.');
            if(empty($data['num'])||$data['num']<0){
                return array('code'=>0,'msg'=>'请输入正确的报单数量');
            }
            if(floor($data['num'])!=$data['num']){
                return array('code'=>0,'msg'=>'请输入正确的报单数量1');
            }
            $info['baodan_time']=time();
            $info['num']=input('num');
            $info['userid']=$_SESSION['think']['user']['id'];
            db('baodan')->insert($info);
            return array('code'=>1,'msg'=>'报单成功，请等待审核');
        }
		 return view();
	}
	public function baodan_record(){
			
		$userid=$_SESSION['think']['user']['id'];
		$lists = db('baodan')->where('userid='.$userid)->order('baodan_time desc')->paginate(20);
		
			 
		$page = $lists->render();
		$this->assign('page', $page);
      
        $this->assign('lists', $lists);
         $this->fetch();
		  return view();
		
	}
	//奖金明细
    public  function  mingxi(){
        $userid=$_SESSION['think']['user']['id'];
        $lists = db('money_log')->where('userid='.$userid)->order('time desc')->paginate(20);


        $page = $lists->render();
        $this->assign('page', $page);

        $this->assign('lists', $lists);
        $this->fetch();
        return view();
    }
    function  tuandui(){
        $userid=input('id');
        if(empty($userid)){
            $userid=$_SESSION['think']['user']['id'];
        }
        $lists = db('users')
            ->alias('u')
            ->join('__USER_LEVEL__ ul','ul.level = u.level','right')
            ->field('u.*,ul.level_name')
            ->where('up_userid',$userid)
            ->order('jihuo_time desc')
            ->paginate(10);
        $page = $lists->render();
        $this->assign('page', $page);
        $this->assign('lists', $lists);
        $this->fetch();
        return view();
    }
    function tuiguang(){

        return view();
    }
    //申请服务中心
    function  application_service(){
        $userid=$_SESSION['think']['user']['id'];
        $user=db('users')->where('id='.$userid)->find();
        if(Request::isAjax()) {
//            $data = input('post.');
//            if(empty($data['num'])||$data['num']<0){
//                return array('code'=>0,'msg'=>'请输入正确的报单数量');
//            }
//            if(floor($data['num'])!=$data['num']){
//                return array('code'=>0,'msg'=>'请输入正确的报单数量1');
//            }
            $info['server']=1;
            db('users')->where('id',$userid)->update($info);
            return array('code'=>1,'msg'=>'申请成功，请等待审核');
        }
        $this->assign('user', $user);
        return view();
    }

}