<?php
//缓存
function savecache($name = '',$id='') {
    if($name=='Field'){
        if($id){
            $Model = db($name);
            $data = $Model->order('sort')->where('moduleid='.$id)->column('*', 'field');
            $name=$id.'_'.$name;
            $data = $data ? $data : null;
            cache($name, $data);
        }else{
            $module = cache('Module');
            foreach ( $module as $key => $val ) {
                savecache($name,$key);
            }
        }
    }elseif($name=='System'){
        $Model = db ( $name );
        $list = $Model->where(array('id'=>1))->find();
        cache($name, $list);
    }elseif($name=='Module'){
        $Model = db ( $name );
        $list = $Model->order('sort')->select ();
        $pkid = $Model->getPk ();
        $data = array ();
        $smalldata= array();
        foreach ( $list as $key => $val ) {
            $data [$val [$pkid]] = $val;
            $smalldata[$val['name']] =  $val [$pkid];
        }
        cache($name, $data);
        cache('Mod', $smalldata);
    }elseif($name == 'cm'){
        $list = db('category')
            ->alias('c')
            ->join('module m','c.moduleid = m.id')
            ->order('c.sort')
            ->field('c.*,m.title as mtitle,m.name as mname')
            ->select();
        cache($name, $list);
    }else{
        $Model = db ($name);
        $list = $Model->order('sort')->select ();
        $pkid = $Model->getPk ();
        $data = array ();
        foreach ( $list as $key => $val ) {
            $data [$val [$pkid]] = $val;
        }
        cache($name, $data);
    }
    return true;
}

//
function style($title_style){
    $title_style = explode(';',$title_style);
    return  $title_style[0].';'.$title_style[1];
}
//请求返回
function callback($status = 0,$msg = '', $url = null, $data = ''){
    $data = array(
        'msg'=>$msg,
        'url'=>$url,
        'data'=>$data,
        'status'=>$status
    );
    return $data;
}

function getvalidate($info){
    $validate_data=array();
    if($info['minlength']) $validate_data['minlength'] = ' minlength:'.$info['minlength'];
    if($info['maxlength']) $validate_data['maxlength'] = ' maxlength:'.$info['maxlength'];
    if($info['required']) $validate_data['required'] = ' required:true';
    if($info['pattern']) $validate_data['pattern'] = ' '.$info['pattern'].':true';
    $errormsg='';
    if($info['errormsg']){
        $errormsg = ' title="'.$info['errormsg'].'"';
    }
    $validate= implode(',',$validate_data);
    $validate= 'validate="'.$validate.'" ';
    $parseStr = $validate.$errormsg;
    return $parseStr;
}
function string2array($info) {
    if($info == '') return array();
    eval("\$r = $info;");
    return $r;
}
function array2string($info) {
    if($info == '') return '';
    if(!is_array($info)){
        $string = stripslashes($info);
    }
    foreach($info as $key => $val){
        $string[$key] = stripslashes($val);
    }
    $setup = var_export($string, TRUE);
    return $setup;
}
//初始表单
function getform($form,$info,$value=''){
    $type = $info['type'];
    return  $form->$type($info,$value);
}
//文件单位换算
function byte_format($input, $dec=0){
    $prefix_arr = array("B", "KB", "MB", "GB", "TB");
    $value = round($input, $dec);
    $i=0;
    while ($value>1024) {
        $value /= 1024;
        $i++;
    }
    $return_str = round($value, $dec).$prefix_arr[$i];
    return $return_str;
}
//时间日期转换
function toDate($time, $format = 'Y-m-d H:i:s') {
    if (empty ( $time )) {
        return '';
    }
    $format = str_replace ( '#', ':', $format );
    return date($format, $time );
}
//地址id转换名称
function toCity($id){
    if (empty ( $id )) {
        return '';
    }
    $name = db('region')->where(['id'=>$id])->value('name');
    return $name;
}
function template_file($module=''){
    $viewPath = config('template.view_path');
    $viewSuffix = config('template.view_suffix');
    $viewPath = $viewPath ? $viewPath : 'view';
    $filepath = think\facade\Env::get('app_path').strtolower(config('default_module')).'/'.$viewPath.'/';
    $tempfiles = dir_list($filepath,$viewSuffix);
    $arr=[];
    foreach ($tempfiles as $key=>$file){
        $dirname = basename($file);
        if($module){
            if(strstr($dirname,$module.'_')) {
                $arr[$key]['value'] =  substr($dirname,0,strrpos($dirname, '.'));
                $arr[$key]['filename'] = $dirname;
                $arr[$key]['filepath'] = $file;
            }
        }else{
            $arr[$key]['value'] = substr($dirname,0,strrpos($dirname, '.'));
            $arr[$key]['filename'] = $dirname;
            $arr[$key]['filepath'] = $file;
        }
    }
    return  $arr;
}
function dir_list($path, $exts = '', $list= array()) {
    $path = dir_path($path);
    $files = glob($path.'*');
    foreach($files as $v) {
        $fileext = fileext($v);
        if (!$exts || preg_match("/\.($exts)/i", $v)) {
            $list[] = $v;
            if (is_dir($v)) {
                $list = dir_list($v, $exts, $list);
            }
        }
    }
    return $list;
}
function dir_path($path) {
    $path = str_replace('\\', '/', $path);
    if(substr($path, -1) != '/') $path = $path.'/';
    return $path;
}
function fileext($filename) {
    return strtolower(trim(substr(strrchr($filename, '.'), 1, 10)));
}
function checkField($table,$value,$field){
    $count = db($table)->where(array($field=>$value))->count();
    if($count>0){
        return true;
    }else{
        return false;
    }
}
/**
+----------------------------------------------------------
 * 产生随机字串，可用来自动生成密码 默认长度6位 字母和数字混合
+----------------------------------------------------------
 * @param string $len 长度
 * @param string $type 字串类型
 * 0 字母 1 数字 其它 混合
 * @param string $addChars 额外字符
+----------------------------------------------------------
 * @return string
+----------------------------------------------------------
 */
function rand_string($len=6,$type='',$addChars='') {
    $str ='';
    switch($type) {
        case 0:
            $chars='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.$addChars;
            break;
        case 1:
            $chars= str_repeat('0123456789',3);
            break;
        case 2:
            $chars='ABCDEFGHIJKLMNOPQRSTUVWXYZ'.$addChars;
            break;
        case 3:
            $chars='abcdefghijklmnopqrstuvwxyz'.$addChars;
            break;
        case 4:
            $chars = "们以我到他会作时要动国产的一是工就年阶义发成部民可出能方进在了不和有大这主中人上为来分生对于学下级地个用同行面说种过命度革而多子后自社加小机也经力线本电高量长党得实家定深法表着水理化争现所二起政三好十战无农使性前等反体合斗路图把结第里正新开论之物从当两些还天资事队批点育重其思与间内去因件日利相由压员气业代全组数果期导平各基或月毛然如应形想制心样干都向变关问比展那它最及外没看治提五解系林者米群头意只明四道马认次文通但条较克又公孔领军流入接席位情运器并飞原油放立题质指建区验活众很教决特此常石强极土少已根共直团统式转别造切九你取西持总料连任志观调七么山程百报更见必真保热委手改管处己将修支识病象几先老光专什六型具示复安带每东增则完风回南广劳轮科北打积车计给节做务被整联步类集号列温装即毫知轴研单色坚据速防史拉世设达尔场织历花受求传口断况采精金界品判参层止边清至万确究书术状厂须离再目海交权且儿青才证低越际八试规斯近注办布门铁需走议县兵固除般引齿千胜细影济白格效置推空配刀叶率述今选养德话查差半敌始片施响收华觉备名红续均药标记难存测士身紧液派准斤角降维板许破述技消底床田势端感往神便贺村构照容非搞亚磨族火段算适讲按值美态黄易彪服早班麦削信排台声该击素张密害侯草何树肥继右属市严径螺检左页抗苏显苦英快称坏移约巴材省黑武培著河帝仅针怎植京助升王眼她抓含苗副杂普谈围食射源例致酸旧却充足短划剂宣环落首尺波承粉践府鱼随考刻靠够满夫失包住促枝局菌杆周护岩师举曲春元超负砂封换太模贫减阳扬江析亩木言球朝医校古呢稻宋听唯输滑站另卫字鼓刚写刘微略范供阿块某功套友限项余倒卷创律雨让骨远帮初皮播优占死毒圈伟季训控激找叫云互跟裂粮粒母练塞钢顶策双留误础吸阻故寸盾晚丝女散焊功株亲院冷彻弹错散商视艺灭版烈零室轻血倍缺厘泵察绝富城冲喷壤简否柱李望盘磁雄似困巩益洲脱投送奴侧润盖挥距触星松送获兴独官混纪依未突架宽冬章湿偏纹吃执阀矿寨责熟稳夺硬价努翻奇甲预职评读背协损棉侵灰虽矛厚罗泥辟告卵箱掌氧恩爱停曾溶营终纲孟钱待尽俄缩沙退陈讨奋械载胞幼哪剥迫旋征槽倒握担仍呀鲜吧卡粗介钻逐弱脚怕盐末阴丰雾冠丙街莱贝辐肠付吉渗瑞惊顿挤秒悬姆烂森糖圣凹陶词迟蚕亿矩康遵牧遭幅园腔订香肉弟屋敏恢忘编印蜂急拿扩伤飞露核缘游振操央伍域甚迅辉异序免纸夜乡久隶缸夹念兰映沟乙吗儒杀汽磷艰晶插埃燃欢铁补咱芽永瓦倾阵碳演威附牙芽永瓦斜灌欧献顺猪洋腐请透司危括脉宜笑若尾束壮暴企菜穗楚汉愈绿拖牛份染既秋遍锻玉夏疗尖殖井费州访吹荣铜沿替滚客召旱悟刺脑措贯藏敢令隙炉壳硫煤迎铸粘探临薄旬善福纵择礼愿伏残雷延烟句纯渐耕跑泽慢栽鲁赤繁境潮横掉锥希池败船假亮谓托伙哲怀割摆贡呈劲财仪沉炼麻罪祖息车穿货销齐鼠抽画饲龙库守筑房歌寒喜哥洗蚀废纳腹乎录镜妇恶脂庄擦险赞钟摇典柄辩竹谷卖乱虚桥奥伯赶垂途额壁网截野遗静谋弄挂课镇妄盛耐援扎虑键归符庆聚绕摩忙舞遇索顾胶羊湖钉仁音迹碎伸灯避泛亡答勇频皇柳哈揭甘诺概宪浓岛袭谁洪谢炮浇斑讯懂灵蛋闭孩释乳巨徒私银伊景坦累匀霉杜乐勒隔弯绩招绍胡呼痛峰零柴簧午跳居尚丁秦稍追梁折耗碱殊岗挖氏刃剧堆赫荷胸衡勤膜篇登驻案刊秧缓凸役剪川雪链渔啦脸户洛孢勃盟买杨宗焦赛旗滤硅炭股坐蒸凝竟陷枪黎救冒暗洞犯筒您宋弧爆谬涂味津臂障褐陆啊健尊豆拔莫抵桑坡缝警挑污冰柬嘴啥饭塑寄赵喊垫丹渡耳刨虎笔稀昆浪萨茶滴浅拥穴覆伦娘吨浸袖珠雌妈紫戏塔锤震岁貌洁剖牢锋疑霸闪埔猛诉刷狠忽灾闹乔唐漏闻沈熔氯荒茎男凡抢像浆旁玻亦忠唱蒙予纷捕锁尤乘乌智淡允叛畜俘摸锈扫毕璃宝芯爷鉴秘净蒋钙肩腾枯抛轨堂拌爸循诱祝励肯酒绳穷塘燥泡袋朗喂铝软渠颗惯贸粪综墙趋彼届墨碍启逆卸航衣孙龄岭骗休借".$addChars;
            break;
        default :
            // 默认去掉了容易混淆的字符oOLl和数字01，要添加请使用addChars参数
            $chars='ABCDEFGHIJKMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz23456789'.$addChars;
            break;
    }
    if($len>10 ) {//位数过长重复字符串一定次数
        $chars= $type==1? str_repeat($chars,$len) : str_repeat($chars,5);
    }
    if($type!=4) {
        $chars   =   str_shuffle($chars);
        $str     =   substr($chars,0,$len);
    }else{
        // 中文随机字
        for($i=0;$i<$len;$i++){
            $str.= msubstr($chars, floor(mt_rand(0,mb_strlen($chars,'utf-8')-1)),1);
        }
    }
    return $str;
}

/**
 * 验证输入的邮件地址是否合法
 */
function is_email($user_email)
{
    $chars = "/^([a-z0-9+_]|\\-|\\.)+@(([a-z0-9_]|\\-)+\\.)+[a-z]{2,6}\$/i";
    if (strpos($user_email, '@') !== false && strpos($user_email, '.') !== false) {
        if (preg_match($chars, $user_email)) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

/**
 * 验证输入的手机号码是否合法
 */
function is_mobile_phone($mobile_phone)
{
    $chars = "/^13[0-9]{1}[0-9]{8}$|15[0-9]{1}[0-9]{8}$|18[0-9]{1}[0-9]{8}$|17[0-9]{1}[0-9]{8}$/";
    if (preg_match($chars, $mobile_phone)) {
        return true;
    }
    return false;
}
/**
 * 取得IP
 *
 * @return string 字符串类型的返回结果
 */
function getIp(){
    if (@$_SERVER['HTTP_CLIENT_IP'] && $_SERVER['HTTP_CLIENT_IP']!='unknown') {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (@$_SERVER['HTTP_X_FORWARDED_FOR'] && $_SERVER['HTTP_X_FORWARDED_FOR']!='unknown') {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return preg_match('/^\d[\d.]+\d$/', $ip) ? $ip : '';
}

//字符串截取
function str_cut($sourcestr,$cutlength,$suffix='...')
{
    $returnstr='';
    $i=0;
    $n=0;
    $str_length=strlen($sourcestr);//字符串的字节数
    while (($n<$cutlength) and ($i<=$str_length))
    {
        $temp_str=substr($sourcestr,$i,1);
        $ascnum=Ord($temp_str);//得到字符串中第$i位字符的ascii码
        if ($ascnum>=224)    //如果ASCII位高与224，
        {
            $returnstr=$returnstr.substr($sourcestr,$i,3); //根据UTF-8编码规范，将3个连续的字符计为单个字符
            $i=$i+3;            //实际Byte计为3
            $n++;            //字串长度计1
        }
        elseif ($ascnum>=192) //如果ASCII位高与192，
        {
            $returnstr=$returnstr.substr($sourcestr,$i,2); //根据UTF-8编码规范，将2个连续的字符计为单个字符
            $i=$i+2;            //实际Byte计为2
            $n++;            //字串长度计1
        }
        elseif ($ascnum>=65 && $ascnum<=90) //如果是大写字母，
        {
            $returnstr=$returnstr.substr($sourcestr,$i,1);
            $i=$i+1;            //实际的Byte数仍计1个
            $n++;            //但考虑整体美观，大写字母计成一个高位字符
        }
        else                //其他情况下，包括小写字母和半角标点符号，
        {
            $returnstr=$returnstr.substr($sourcestr,$i,1);
            $i=$i+1;            //实际的Byte数计1个
            $n=$n+0.5;        //小写字母和半角标点等与半个高位字符宽...
        }
    }
    if ($n>$cutlength){
        $returnstr = $returnstr . $suffix;//超过长度时在尾处加上省略号
    }
    return $returnstr;
}
//删除目录及文件
function dir_delete($dir) {
    $dir = dir_path($dir);
    if (!is_dir($dir)) return FALSE;
    $list = glob($dir.'*');
    foreach($list as $v) {
        is_dir($v) ? dir_delete($v) : @unlink($v);
    }
    return @rmdir($dir);
}
/**
 * CURL请求
 * @param $url 请求url地址
 * @param $method 请求方法 get post
 * @param null $postfields post数据数组
 * @param array $headers 请求header信息
 * @param bool|false $debug  调试开启 默认false
 * @return mixed
 */
function httpRequest($url, $method, $postfields = null, $headers = array(), $debug = false) {
    $method = strtoupper($method);
    $ci = curl_init();
    /* Curl settings */
    curl_setopt($ci, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    curl_setopt($ci, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:34.0) Gecko/20100101 Firefox/34.0");
    curl_setopt($ci, CURLOPT_CONNECTTIMEOUT, 60); /* 在发起连接前等待的时间，如果设置为0，则无限等待 */
    curl_setopt($ci, CURLOPT_TIMEOUT, 7); /* 设置cURL允许执行的最长秒数 */
    curl_setopt($ci, CURLOPT_RETURNTRANSFER, true);
    switch ($method) {
        case "POST":
            curl_setopt($ci, CURLOPT_POST, true);
            if (!empty($postfields)) {
                $tmpdatastr = is_array($postfields) ? http_build_query($postfields) : $postfields;
                curl_setopt($ci, CURLOPT_POSTFIELDS, $tmpdatastr);
            }
            break;
        default:
            curl_setopt($ci, CURLOPT_CUSTOMREQUEST, $method); /* //设置请求方式 */
            break;
    }
    $ssl = preg_match('/^https:\/\//i',$url) ? TRUE : FALSE;
    curl_setopt($ci, CURLOPT_URL, $url);
    if($ssl){
        curl_setopt($ci, CURLOPT_SSL_VERIFYPEER, FALSE); // https请求 不验证证书和hosts
        curl_setopt($ci, CURLOPT_SSL_VERIFYHOST, FALSE); // 不从证书中检查SSL加密算法是否存在
    }
    //curl_setopt($ci, CURLOPT_HEADER, true); /*启用时会将头文件的信息作为数据流输出*/
    //curl_setopt($ci, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ci, CURLOPT_MAXREDIRS, 2);/*指定最多的HTTP重定向的数量，这个选项是和CURLOPT_FOLLOWLOCATION一起使用的*/
    curl_setopt($ci, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ci, CURLINFO_HEADER_OUT, true);
    /*curl_setopt($ci, CURLOPT_COOKIE, $Cookiestr); * *COOKIE带过去** */
    $response = curl_exec($ci);
    $requestinfo = curl_getinfo($ci);
    $http_code = curl_getinfo($ci, CURLINFO_HTTP_CODE);
    if ($debug) {
        echo "=====post data======\r\n";
        var_dump($postfields);
        echo "=====info===== \r\n";
        print_r($requestinfo);
        echo "=====response=====\r\n";
        print_r($response);
    }
    curl_close($ci);
    return $response;
    //return array($http_code, $response,$requestinfo);
}
/**
 * @param $arr
 * @param $key_name
 * @return array
 * 将数据库中查出的列表以指定的 id 作为数组的键名
 */
function convert_arr_key($arr, $key_name)
{
    $arr2 = array();
    foreach($arr as $key => $val){
        $arr2[$val[$key_name]] = $val;
    }
    return $arr2;
}
//查询IP地址
function getCity($ip = ''){
    $res = @file_get_contents('http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=js&ip=' . $ip);
    if(empty($res)){ return false; }
    $jsonMatches = array();
    preg_match('#\{.+?\}#', $res, $jsonMatches);
    if(!isset($jsonMatches[0])){ return false; }
    $json = json_decode($jsonMatches[0], true);
    if(isset($json['ret']) && $json['ret'] == 1){
        $json['ip'] = $ip;
        unset($json['ret']);
    }else{
        return false;
    }
    return $json;
}
//判断图片的类型从而设置图片路径
function imgUrl($img,$defaul=''){
    if($img){
        if(substr($img,0,4)=='http'){
            $imgUrl = $img;
        }else{
            $imgUrl = $img;
        }
    }else{
        if($defaul){
            $imgUrl = $defaul;
        }else{
            $imgUrl = '/static/admin/images/tong.png';
        }

    }
    return $imgUrl;
}
/**
 * PHP格式化字节大小
 * @param  number $size      字节数
 * @param  string $delimiter 数字和单位分隔符
 * @return string            格式化后的带单位的大小
 */
function format_bytes($size, $delimiter = '') {
    $units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
    for ($i = 0; $size >= 1024 && $i < 5; $i++) $size /= 1024;
    return round($size, 2) . $delimiter . $units[$i];
}
/**
 * 判断当前访问的用户是  PC端  还是 手机端  返回true 为手机端  false 为PC 端
 *  是否移动端访问访问
 * @return boolean
 */
 function picshow($pics){
	$a=' "/></div><div class="swiper-slide"><img src="';
	$arr=str_replace(";",$a,$pics);
	$arr='<div class="swiper-slide"><img src="'.$arr.'"/></div>';
	echo   htmlspecialchars_decode($arr);
}
function isMobile()
{
    // 如果有HTTP_X_WAP_PROFILE则一定是移动设备
    if (isset ($_SERVER['HTTP_X_WAP_PROFILE']))
        return true;

    // 如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
    if (isset ($_SERVER['HTTP_VIA']))
    {
        // 找不到为flase,否则为true
        return stristr($_SERVER['HTTP_VIA'], "wap") ? true : false;
    }
    // 脑残法，判断手机发送的客户端标志,兼容性有待提高
    if (isset ($_SERVER['HTTP_USER_AGENT']))
    {
        $clientkeywords = array ('nokia','sony','ericsson','mot','samsung','htc','sgh','lg','sharp','sie-','philips','panasonic','alcatel','lenovo','iphone','ipod','blackberry','meizu','android','netfront','symbian','ucweb','windowsce','palm','operamini','operamobi','openwave','nexusone','cldc','midp','wap','mobile');
        // 从HTTP_USER_AGENT中查找手机浏览器的关键字
        if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT'])))
            return true;
    }
    // 协议法，因为有可能不准确，放到最后判断
    if (isset ($_SERVER['HTTP_ACCEPT']))
    {
        // 如果只支持wml并且不支持html那一定是移动设备
        // 如果支持wml和html但是wml在html之前则是移动设备
        if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html'))))
        {
            return true;
        }
    }
    return false;
}


function is_weixin() {
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) {
        return true;
    } return false;
}

function is_qq() {
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'QQ') !== false) {
        return true;
    } return false;
}
function is_alipay() {
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'AlipayClient') !== false) {
        return true;
    } return false;
}

/**
 * 获取用户信息
 * @param $user_id_or_name  用户id 邮箱 手机 第三方id
 * @param int $type  类型 0 user_id查找 1 邮箱查找 2 手机查找 3 第三方唯一标识查找
 * @param string $oauth  第三方来源
 * @return mixed
 */
function get_user_info($user_id_or_name,$type = 0,$oauth=''){
    $map = array();
    if($type == 0){
        $map[] = ['user_id','=',$user_id_or_name];
    }
    if($type == 1){
        $map[] = ['email','=',$user_id_or_name];
    }
    if($type == 2){
        $map[] = ['mobile','=',$user_id_or_name];
    }
    if($type == 3){
        $map[] = ['openid','=',$user_id_or_name];
        $map[] = ['oauth','=',$oauth];
    }
    if($type == 4){
        $map[] = ['unionid','=',$user_id_or_name];
        $map[] = ['oauth','=',$oauth];
    }
    if($type == 5){
        $map[] = ['nickname','=',$user_id_or_name];
    }
    $user = db('users')->where($map)->find();
    return $user;
}
/**
 * 过滤数组元素前后空格 (支持多维数组)
 * @param $array 要过滤的数组
 * @return array|string
 */
function trim_array_element($array){
    if(!is_array($array))
        return trim($array);
    return array_map('trim_array_element',$array);
}
/**
 * @param $arr
 * @param $key_name
 * @return array
 * 将数据库中查出的列表以指定的 值作为数组的键名，并以另一个值作为键值
 */
function convert_arr_kv($arr,$key_name,$value){
    $arr2 = array();
    foreach($arr as $key => $val){
        $arr2[$val[$key_name]] = $val[$value];
    }
    return $arr2;
}
/**
 * 邮件发送
 * @param $to    接收人
 * @param string $subject   邮件标题
 * @param string $content   邮件内容(html模板渲染后的内容)
 * @throws Exception
 * @throws phpmailerException
 */
function send_email($to,$subject='',$content=''){
    $mail = new PHPMailer\PHPMailer\PHPMailer();
    $arr = db('config')->where('inc_type','smtp')->select();
    $config = convert_arr_kv($arr,'name','value');

    $mail->CharSet  = 'UTF-8'; //设定邮件编码，默认ISO-8859-1，如果发中文此项必须设置，否则乱码
    $mail->isSMTP();
    $mail->SMTPDebug = 0;
    //调试输出格式
    //$mail->Debugoutput = 'html';
    //smtp服务器
    $mail->Host = $config['smtp_server'];
    //端口 - likely to be 25, 465 or 587
    $mail->Port = $config['smtp_port'];

    if($mail->Port == '465') {
        $mail->SMTPSecure = 'ssl';
    }// 使用安全协议
    //Whether to use SMTP authentication
    $mail->SMTPAuth = true;
    //发送邮箱
    $mail->Username = $config['smtp_user'];
    //密码
    $mail->Password = $config['smtp_pwd'];
    //Set who the message is to be sent from
    $mail->setFrom($config['smtp_user'],$config['email_id']);
    //回复地址
    //$mail->addReplyTo('replyto@example.com', 'First Last');
    //接收邮件方
    if(is_array($to)){
        foreach ($to as $v){
            $mail->addAddress($v);
        }
    }else{
        $mail->addAddress($to);
    }

    $mail->isHTML(true);// send as HTML
    //标题
    $mail->Subject = $subject;
    //HTML内容转换
    $mail->msgHTML($content);
    return $mail->send();
}
//积分计算函数
//  koufei($type,$userid,$num，$source,$jiajian)
//  type          1为积分    2为 金钱    3 为本金  4 为其他
//	$source       1商城消费  2 会员提现  3会员转账  4 管理变动  5 手动充值  6 微信充值 7
//  num           数值
//  $jiajian     1 为 加       2为减
//  返回结果     1、执行成功   2 余额不足。
function jifen($type,$userid,$num,$source,$jiajian,$beizhu){
	
		$typename=db('money_config')->where('id='.$type)->value('varname');
		
	
	//执行加减计算	
	$user=db('users')->where('id='.$userid)->find();
	if($jiajian==1){
			$xianxian[$typename]=$user[$typename]+$num;
			$numx='+';
		
	}
	if($jiajian==2){
			////判断余额是否充足
			if($num>$user[$typename]){
					return 2;
			}
			$xianxian[$typename]=$user[$typename]-$num;
			$numx='-';
	}
	//
	
	db('users')->where('id='.$userid)->update($xianxian);
	
	//增加积分变动日志
	$addinfo['type']=$type; 
	$addinfo['userid']=$userid;
	$addinfo['dangqian']=$xianxian[$typename];
	$addinfo['num']=$numx.$num;
	$addinfo['source']=$source;
	$addinfo['time']=time();
	if(!empty($beizhu)){
		$addinfo['beizhu']=$beizhu;
		
	}
	
	
	db('money_log')->insert($addinfo);
	return 1;
	
	
}
//传 金币ID 以及 用户名，返回余额
function getyue($typeid,$userid){
			
			$varname=db('money_config')->where('id='.$typeid)->value('varname');
			$yue=db('users')->where('id='.$userid)->value($varname);
			return $yue;
		
	
}
function tixiantype($typeid){
    $varname=db('money_config')->where('id='.$typeid)->value('name');
    return $varname;
}
function money_log_type($typeid){
    $varname=db('money_type')->where('id',$typeid)->value('type');
    return $varname;
}
function order_title($typeid){
		$varname=db('shop')->where('id',$typeid)->value('title');
		return $varname;
}
function order_price($typeid){
		$varname=db('shop')->where('id',$typeid)->value('jiage');
		return $varname;
}
function order_img($typeid){
		$varname=db('shop')->where('id',$typeid)->value('thumb');
		return $varname;
}

//根据会员id获取会员信息
function getuser($id){
    $user=db('users')->where(array('id'=>$id))->find();
    return $user;
}
//根据会员等级查询各类奖项比例
function  getulevel($level){
    $lv=db('user_level')->where(array('level'=>$level))->find();
    return $lv;
}
function jihuo($id,$num){
    $user=getuser($id);
    $time=time();
    Db('users')->where('id',$user['id'])->setField('jihuo_time', $time);
    Db('users')->where('id', $user['id'])->setInc('dan',$num);
    $level=getulevel($user['level']);
    achievement($num*$level['lsk'],$num*$level['lsk'],1,1,0,0,0,0,0);
    addarea($user['fatherid'],$level['lsk'],$user['treeplace']);//增加业绩
    if(!empty($user['up_userid'])){
        Db('users')->where('id', $user['up_userid'])->setInc('recount',$num);
        //addachievement($user['up_userid'],$num);
        /*        bonus1($user['up_userid'],$user['mobile'],$num);*/
    }
    if(!empty($user['fatherid'])){
        bonus2($user['id']);
    }

/*    if(!empty($user['rpath'])){
        lirunjiang($user['id'],$num);
        uplevel($user['id']);
    }*/
/*    if(!empty($user['center_id'])){
        baodanjiang($user['center_id'],$num,$user['mobile']);
    }*/
   // fenhong($id);
}
//直推奖
function bonus1($id,$nickname,$num){
    if($id){
        $user=getuser($id);
        $level=getulevel($user['level']);
        $money=$level['zhitui']*$num;
        if($money>0){
            achievement(0,0,0,0,$money,$money,$money,0,0);
            bonusdel($user['id'],$money);
            jifen(1,$user['id'],$money,9,1,"直推奖".$nickname);

        }
    }
}
//见点奖
function bonus2($id){
        $user=getuser($id);
        $pusers=db('users')
            ->where('id','in',$user['ppath'])
            ->where('plevel','between',[0,$user['plevel']])
            ->select();
        if(!empty($pusers)){
            foreach ($pusers as $key=>$v){
                $poor=$user['plevel']-$v['plevel'];
                $level=getulevel($v['level']);
                if($poor==1){
                    $money=$level['b1']/100*$level['lsk'];
                    if($money>0){
                        jifen(1,$v['uid'],$money,12,1,$user['mobile']);
                        achievement(0,0,0,0,$money,$money,$money,0,0);
                    //    bonusdel($v['uid'],$money);
                    }
                }
                if($v['is_b']==0&&$v['mobile']!="18353999999"){
                    bonus3($v['uid'],$user['plevel']);
                }
            }
        }
}


//判断是否能进B网
function bonus3($id,$plevel){
        $vppath=db('users')->where('id',$id)->value('ppath');
        $addvpath=$vppath.$id;
        $num=db('users')
            ->where('ppath','like','%'.$addvpath.'%')
            ->where('plevel',$plevel)
            ->where('activation',1)
            ->count();
        if($num>=4){
           jinb($id);
        }
}
//进B网操作
function jinb($id){
    $user=getuser($id);
    $buser=db('busers')
        ->where('left',0)
        ->whereOr('center',0)
        ->whereOr('right',0)
        ->order('plevel asc,id asc')
        ->find();
    if(empty($buser)){
        $add_buser=array(
            'uid'=>$id,
            'ppath'=>',',
            'level'=>1,
            'mobile'=>$user['mobile'],
        );
        $userId =db('busers')->insertGetId($add_buser);
        bonus4($userId);
    }else{
        if($buser['left']==0){
            $treeplace=1;

        }elseif($buser['center']==0){
            $treeplace=2;
        }else{
            $treeplace=3;
        }
        $add_buser=array(
            'uid'=>$id,
            'treeplace'=>$treeplace,
            'ppath'=>$buser['ppath'].$buser['id'].",",
            'plevel'=>$buser['plevel']+1,
            'fatherid'=>$buser['id'],
            'level'=>$user['level'],
            'mobile'=>$user['mobile'],
        );
        /*            db('busers')->insert($add_buser);*/
        $bid = db('busers')->insertGetId($add_buser);
        if($treeplace==1){
            db('busers')->where('id',$buser['id'])->setField('left',$bid);
        }elseif($treeplace==2){
            db('busers')->where('id',$buser['id'])->setField('center',$bid);
        }elseif($treeplace==3){
            db('busers')->where('id',$buser['id'])->setField('right',$bid);
        }
        bonus4($bid);
    }
    db('users')->where('id',$user['id'])->setInc('is_b');

}
//进b网发放奖金
function bonus4($bid){
    $buser=db('busers')->where('id',$bid)->find();
    $pusers=db('busers')
        ->where('id','in',$buser['ppath'])
        ->where('plevel','between',[0,$buser['plevel']])
        ->select();
    if(!empty($pusers)){
        foreach ($pusers as $key=>$v){
            $poor=$buser['plevel']-$v['plevel'];
            $level=getulevel($v['level']);
            if($poor==1){
                $money=$level['b2'];
                if($money>0){
                    jifen(1,$v['uid'],$money,13,1,$buser['mobile']);
                    achievement(0,0,0,0,$money,$money,0,$money,0);
                //    bonusdel($v['uid'],$money);
                }
            }elseif($poor==2){
                $money=$level['b3'];
                if($money>0){
                    jifen(1,$v['uid'],$money,13,1,$buser['mobile']);
                    achievement(0,0,0,0,$money,$money,0,$money,0);
                //    bonusdel($v['uid'],$money);
                    if($v['is_buckle']==0){
                        jifen(1,$v['uid'],2000,11,2,$v['mobile']);
                        db('busers')->where('id',$v['id'])->setInc('is_buckle');
                    }
                }
            }
            if($v['is_a']==0&&$v['mobile']!="18353999999"){
                bonus5($v['id'],$buser['plevel']);
            }
        }
    }
}

//判断是否能进A网
function bonus5($bid,$plevel){
    $vppath=db('busers')->where('id',$bid)->value('ppath');
    $addvppath=$vppath.$bid;
    $num=db('busers')
        ->where('ppath','like','%'.$addvppath.'%')
        ->where('plevel',$plevel)
        ->count();
    if($num>=9){
        jina($bid);
    }
}
//进a网
function jina($bid){
    $buser=db('busers')->where('id',$bid)->find();
   // $user=getuser($buser['uid']);
    $user=db('users')
        ->where('leftid',0)
        ->whereOr('rightid',0)
        ->order('plevel asc,id asc')
        ->find();
    if(empty($user)){
        $add_buser=array(
            'uid'=>$buser['uid'],
            'ppath'=>',',
            'level'=>$user['level'],
            'mobile'=>$user['mobile'],
        );
        db('users')->insert($add_buser);
        $userId = db('users')->getLastInsID();
       // bonus4($userId);
    }else{
        if($user['leftid']==0){
            $treeplace=1;
        }elseif($user['rightid']==0){
            $treeplace=2;
        }
        $add_buser=array(
            'uid'=>$buser['uid'],
            'treeplace'=>$treeplace,
            'ppath'=>$user['ppath'].$user['id'].",",
            'plevel'=>$user['plevel']+1,
            'fatherid'=>$user['id'],
            'level'=>$buser['level'],
            'mobile'=>$buser['mobile'],
            'activation'=>1,
        );
/*        db('users')->insert($add_buser);*/
        $id = db('users')->insertGetId($add_buser);
        if($treeplace==1){
            db('users')->where('id',$user['id'])->setField('leftid',$id);
        }else{
            db('users')->where('id',$user['id'])->setField('rightid',$id);
        }
     /*   $id = db('users')->getLastInsID();*/
        bonus2($id);
    }
    db('users')->where('id',$user['id'])->setInc('is_b');
}



//升级
function uplevel($id){
        $user=getuser($id);
        $rpath=trim($user['rpath'],",");
        $rpatharr=explode(",",$rpath);
        if(!empty($rpatharr)){
            foreach ($rpatharr as $key=>$value){
                $vuser=getuser($value);
                $level=getulevel($vuser['level']+1);
                if($level['level']<4){
                    if($vuser['recount']>=$level['bomlimit']&&$vuser['achievement']>=$level['achievement']){
                        db('users')->where('id', $vuser['id'])->setInc('level');
                    }
                }elseif($level['level']>=4&&$level['level']<8) {
                    if ($vuser['achievement'] >= $level['achievement']) {
                        db('users')->where('id', $vuser['id'])->setInc('level');
                    }
                }
            }
        }
}

//极差奖
function lirunjiang($id,$num){
    $user=getuser($id);
    $rpath=trim($user['rpath'],",");
    $rpatharr=explode(",",$rpath);
    $path=array_reverse($rpatharr);
    $a=0;
    $b=0;
    $c=0;
    $d=0;
    $e=0;
    $f=0;
    $g=0;
    foreach ($path as $key=>$value){
        $money=0;
        $vuser=getuser($value);
        $level=getulevel($vuser['level']);
        if($vuser['level']==2){
            if($a==0&&$b==0&&$c==0&&$d==0&&$e==0&&$f==0&&$g==0){
                  $money=$level['lirun'];
                  $a=1;
            }
        }elseif($vuser['level']==3){
            if($b==0&&$c==0&&$d==0&&$e==0&&$f==0&&$g==0){
                if($a==0){
                    $money=$level['lirun'];
                }else{
                    $level2=getulevel($vuser['level']-1);
                    $money=$level['lirun']-$level2['lirun'];
                }
                $b=1;
            }
        }elseif($vuser['level']==4&&$c==0&&$d==0&&$e==0&&$f==0&&$g==0){
            if($b==1){
                    $level2=getulevel($vuser['level']-1);
                    $money=$level['lirun']-$level2['lirun'];
            }elseif($a==1&&$b==0){
                $level2=getulevel($vuser['level']-2);
                $money=$level['lirun']-$level2['lirun'];
            }else{
                $money=$level['lirun'];
            }
            $c=1;
        }elseif($vuser['level']==5&&$d==0&&$e==0&&$f==0&&$g==0){
            if($c==1){
                $level2=getulevel($vuser['level']-1);
                $money=$level['lirun']-$level2['lirun'];
            }elseif($b==1){
                $level2=getulevel($vuser['level']-2);
                $money=$level['lirun']-$level2['lirun'];
            }elseif($a==1){
                $level2=getulevel($vuser['level']-3);
                $money=$level['lirun']-$level2['lirun'];
            }else{
                $money=$level['lirun'];
            }
            $d=1;
        }elseif($vuser['level']==6&&$e==0&&$f==0&&$g==0){
            if($d==1){
                $level2=getulevel($vuser['level']-1);
                $money=$level['lirun']-$level2['lirun'];
            }elseif($c==1){
                $level2=getulevel($vuser['level']-2);
                $money=$level['lirun']-$level2['lirun'];
            }elseif($b==1){
                $level2=getulevel($vuser['level']-3);
                $money=$level['lirun']-$level2['lirun'];
            }elseif($a==1){
                $level2=getulevel($vuser['level']-4);
                $money=$level['lirun']-$level2['lirun'];
            }else{
                $money=$level['lirun'];
            }
            $e=1;
        }elseif($vuser['level']==7&&$f==0&&$g==0){
            if($e==1){
                $level2=getulevel($vuser['level']-1);
                $money=$level['lirun']-$level2['lirun'];
            }elseif($d==1){
                $level2=getulevel($vuser['level']-2);
                $money=$level['lirun']-$level2['lirun'];
            }elseif($c==1){
                $level2=getulevel($vuser['level']-3);
                $money=$level['lirun']-$level2['lirun'];
            }elseif($b==1){
                $level2=getulevel($vuser['level']-4);
                $money=$level['lirun']-$level2['lirun'];
            }elseif($a==1){
                $level2=getulevel($vuser['level']-5);
                $money=$level['lirun']-$level2['lirun'];
            }else{
                $money=$level['lirun'];
            }
            $f=1;
        }elseif($vuser['level']==8&&$g==0){
            if($f==1){
                $level2=getulevel($vuser['level']-1);
                $money=$level['lirun']-$level2['lirun'];
            }elseif($e==1){
                $level2=getulevel($vuser['level']-2);
                $money=$level['lirun']-$level2['lirun'];
            }elseif($d==1){
                $level2=getulevel($vuser['level']-3);
                $money=$level['lirun']-$level2['lirun'];
            }elseif($c==1){
                $level2=getulevel($vuser['level']-4);
                $money=$level['lirun']-$level2['lirun'];
            }elseif($b==1){
                $level2=getulevel($vuser['level']-5);
                $money=$level['lirun']-$level2['lirun'];
            }elseif($a==1){
                $level2=getulevel($vuser['level']-6);
                $money=$level['lirun']-$level2['lirun'];
            }else{
                $money=$level['lirun'];
            }
            $g=1;
        }
        $money=$money*$num;
        if($money>0){
            bonusdel($vuser['id'],$money);
            achievement(0,0,0,0,$money,$money,0,$money,0);
            jifen(1,$vuser['id'],$money,9,1,"利润奖".$user['mobile']);
        }
    }
}

//增加伞下业绩（伞下总业绩）
function addachievement($up_userid,$num){
    $up_user=getuser($up_userid);
    if(!empty($up_user)){
        db('users')->where('id', $up_userid)->setInc('achievement',$num);
        addachievement($up_user['up_userid'],$num);
    }

}
function addarea($fatherid,$lsk,$treeplace){
    $fatheruser=getuser($fatherid);
    if(!empty($fatheruser)){
        if($treeplace==1){
            $area1=$fatheruser['area1']+$lsk;
            $narea1=$fatheruser['narea1']+$lsk;
            $date=array(
                'area1'=>$area1,
                'narea1'=>$narea1,
            );
        }else{
            $area2=$fatheruser['area2']+$lsk;
            $narea2=$fatheruser['narea2']+$lsk;
            $date=array(
                'area2'=>$area2,
                'narea2'=>$narea2,
            );
        }
        db('users')->where('id', $fatherid)->update($date);
        addachievement($fatheruser['fatherid'],$lsk);
    }
}
//报单奖
function baodanjiang($serverid,$num,$mobile){
    $server=getuser($serverid);
    $level=getulevel($server['level']);
    $money=$level['baodan']*$num;
    if($money>0){
        achievement(0,0,0,0,$money,$money,0,0,0,$money);
        bonusdel($server['id'],$money);
        jifen(1,$server['id'],$money,14,1,"报单奖".$mobile);
    }
}
//分红奖
function fenhong($id){
    $user=getuser($id);
    $level=db('users')
        ->where('level',7)
/*        ->whereOr('level',8)*/
        ->select();
    $count=db('users')
        ->where('level',7)
 /*       ->whereOr('level',8)*/
        ->count();
    foreach ($level as $key=>$value){
        $level=getulevel($value['level']);
        $money=$level['lsk']*$level['fenhong']/100/$count;
        if($money>0){
            jifen(1,$value['id'],$money,9,1,"省代理分红奖".$user['mobile']);
        }
    }

}
//业绩统计
function achievement($a,$b,$c,$d,$e,$f,$g,$h,$i,$j=0){
    $createtime=strtotime(date('Y-m-d'));
    $achievement=db('total_achievement')->where('createtime',$createtime)->find();
    if(!empty($achievement)){
        $achievement_change['createtime']=$createtime;
        $achievement_change['newadd_achievement']=$achievement['newadd_achievement']+$a;
        $achievement_change['all_achievement']=$achievement['all_achievement']+$b;
        $achievement_change['newadd_user']=$achievement['newadd_user']+$c;
        $achievement_change['all_user']=$achievement['all_user']+$d;
        $achievement_change['new_grant']=$achievement['new_grant']+$e;
        $achievement_change['all_grant']=$achievement['all_grant']+$f;
        $achievement_change['bonus1']=$achievement['bonus1']+$g;
        $achievement_change['bonus2']=$achievement['bonus2']+$h;
        $achievement_change['bonus3']=$achievement['bonus3']+$i;
        $achievement_change['bonus4']=$achievement['bonus4']+$j;
        db('total_achievement')->where('id',$achievement['id'])->update($achievement_change);
    }else{
        $all_achievement=db('total_achievement')->where('id','gt',0)->sum('all_achievement');
        $all_user=db('total_achievement')->where('id','gt',0)->sum('all_user');
        $all_grant=db('total_achievement')->where('id','gt',0)->sum('all_grant');
        $achievement_change['createtime']=$createtime;
        $achievement_change['newadd_achievement']=$a;
        $achievement_change['all_achievement']=$b+$all_achievement;
        $achievement_change['newadd_user']=$c;
        $achievement_change['all_user']=$d+$all_user;
        $achievement_change['new_grant']=$e;
        $achievement_change['all_grant']=$f+$all_grant;
        $achievement_change['bonus1']=$g;
        $achievement_change['bonus2']=$h;
        $achievement_change['bonus3']=$i;
        $achievement_change['bonus4']=$j;
        db('total_achievement')->where('id',$achievement['id'])->insert($achievement_change);
    }
}
//奖金发放时扣除手续费
function  bonusdel($id,$money){
    $mobile=db('users')->where('id',$id)->value('mobile');
    $sx=db('system')->where('id',1)->value('shouxufei');
    $sxmoney=$money*$sx/100;
    jifen(1,$id,$sxmoney,10,2,$mobile);
}

