<?php
namespace app\mobile\controller;
use clt\Lunar;
class Shop extends Common{
    public function initialize(){
        parent::initialize();
    }
    public function info(){
		$id=input('id');
		$info=db('shop')->where('id='.$id)->find();
		
		
			
        $this->assign('info',$info);
		 return view();
    }
	 public function lists(){
		 $type=input('catId');
		$lists = db('shop')->where('catid='.$type)->paginate(20);
		$page = $lists->render();
		$this->assign('page', $page);
        $this->assign('lists', $lists);
         $this->fetch();
		 return view();
    }
}