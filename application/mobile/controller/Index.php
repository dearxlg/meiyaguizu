<?php
namespace app\mobile\controller;
use clt\Lunar;
class Index extends Common{
    public function initialize(){
        parent::initialize();
    }
    public function index(){
		
		$ad=db('ad')->where('open=1')->limit(5)->order('sort desc')->select();
	
		
		  $this->assign('ad',$ad);
		
		$nav=db('category')->where('parentid=3')->limit(7)->order('sort desc')->select();
	
		
		  $this->assign('nav',$nav);
		
        return view();
    }
}