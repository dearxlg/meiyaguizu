<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/8/23
 * Time: 15:43
 */
namespace app\admin\controller;
use think\Db;
use think\facade\Request;
class Staticstate extends Common
{
    public function initialize()
    {
        parent::initialize();
    }
    public function index()
    {
        $User=db('users')->where('level','gt',4)->select();
        foreach ($User as $key=>$value){
            if(!empty($value['up_userid'])){
                $level=getulevel($value['level']);
                $money=$level['b1'];
                if($money>0){
                    achievement(0,0,0,0,$money,$money,0,0,$money);
                    bonusdel($value['up_userid'],$money);
                    jifen(1,$value['up_userid'],$money,9,1,"培育奖".$value['mobile']);
                }
            }
        }
    }
}