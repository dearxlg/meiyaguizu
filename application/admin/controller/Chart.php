<?php
/**
 * Created by PhpStorm.
 * User:wyy
 * Date: 2018/8/28
 * Time: 10:00
 */

namespace app\admin\controller;


use think\Db;

class Chart extends Common
{
    public function index(){
        // 获取网络数据
        $this->fetchByType('11');

        // 推荐网络类型
        $this->assign('type', 1);

        return view();
    }
    // 推荐关系数据
    private function fetchByType($type)
    {

        // 用户编码
        $username = input('mobile') ? input('mobile') : '18353999999';
        $id = input('id') ? input('id') : '1';

        // 显示层级
        $level =intval(input('plevel')) ? intval(input('plevel')) : 3;

        $this->assign('plevel', $level);

        // 网络数据
        $network = [];
        $this->getNodeNetwork($network,$id, $username, $level);
//        if ($type == SQJ_NETWORK_RECOMMEND)
//        {
//            (new User())->getRegisterNetwork($network, $userCode, $level);
//        }
//        else
//        {
//            (new User())->getNodeNetwork($network, $userCode, $level);
//        }
/*        var_dump($network);*/
        $this->assign('network', json_encode($network,true));
    }

    public function getNodeNetwork(&$network,
                                   $id,
                                   $username,
                                   $maxLevel,
                                   $currentLevel = 1,
                                   $parent = 0)
    {
        // 如果层数大于等于最大层数时，则不执行任何操作
        if ($currentLevel + 1 > $maxLevel) {
            return;
        }

        if ($currentLevel == 1) {
            $user = Db::name('users')->alias('su')
                ->join('__USER_LEVEL__ nm','su.level = nm.level',"LEFT")
                ->where('su.mobile', $username)
                ->find();
        //    var_dump($user);
            $network[] = [
                'key' => 0,
                'mobile' => $username,
                'level_name' => $user['level_name'],
                'leftuse' => $user['area1'],
                'rightuse' =>$user['area2'],
              //  'myuse'=>$myuse,
                'is_active'=>1,
                'floor' => '第【' . $currentLevel . '】层'
            ];
        }else{
            $user = Db::name('users')->alias('su')
                ->join('__USER_LEVEL__ nm','su.level = nm.level',"LEFT")
                ->where('su.mobile', $username)
                ->where('id',$id)
                ->find();
        }
        //var_dump($network);die();
        //var_dump(122);die();
        if($user['leftid'] !=0){
            $userone = Db::name('users')->alias('su')
                ->join('__USER_LEVEL__ nm','su.level = nm.level',"LEFT")
                ->where('su.id',$user['leftid'])
                ->find();
          //  var_dump($userone);
            $list[0] = array('mobile' => $userone['mobile'],
                'level_name' => $userone['level_name'],
                'leftuse' => $userone['area1'],
                'rightuse' => $userone['area2'],
                'id' => $userone['id'],
              //  'myuse'=>$myuseone
            );
        }else{
            $list[0] = array();
        }

        if($user['rightid']!=0){
            $usertwo =Db::name('users')->alias('su')
                ->join('__USER_LEVEL__ nm','su.level = nm.level',"LEFT")
                ->where('su.id',$user['rightid'])
                ->find();

            $list[1] = array('mobile' => $usertwo['mobile'],
                'level_name' => $usertwo['level_name'],
                'leftuse' => $usertwo['area1'],
                'rightuse' =>$usertwo['area2'],
                'id' =>$usertwo['id'],
                );
        }else{
            $list[1] = array();
        }
       // $list[2] = array();
        // 不存在推荐会员则不执行任何操作
        if (empty($list[0])&&empty($list[1])) {
		//	var_dump($network);die();
            return;
        }
        // 遍历所有下线，获取数据
        foreach ($list as $item) {
            $key = count($network);

            $network[] = [
                'key' => $key,
                'parent' => $parent,
                'mobile' => $item['mobile'],
                'level_name' => $item['level_name'],
                'leftuse' => $item['leftuse'],
                'rightuse' => $item['rightuse'],
                'id' => $item['id'],
                'is_active'=>1,
                'floor' => '第【' . ($currentLevel + 1) . '】层'
            ];
//            var_dump($network);
            $this->getNodeNetwork($network, $item['id'], $item['mobile'], $maxLevel, ++$currentLevel, $key);

            --$currentLevel;
        }
    }
    public  function  bindex(){
        // 获取网络数据
        $this->bfetchByType('11');

        // 推荐网络类型
        $this->assign('type', 1);

        return view();


    }

    // 推荐关系数据
    private function bfetchByType($type)
    {

        // 用户编码
        $username = input('mobile') ? input('mobile') : '18353999999';
        $sid=db('busers')->order('id asc')->value('id');
        $id = input('id') ? input('id') : $sid;

        // 显示层级
        $level =intval(input('plevel')) ? intval(input('plevel')) : 3;

        $this->assign('plevel', $level);

        // 网络数据
        $network = [];
        $this->bgetNodeNetwork($network, $id,$username, $level);
//        if ($type == SQJ_NETWORK_RECOMMEND)
//        {
//            (new User())->getRegisterNetwork($network, $userCode, $level);
//        }
//        else
//        {
//            (new User())->getNodeNetwork($network, $userCode, $level);
//        }
/*        var_dump($network);*/
        $this->assign('network', json_encode($network,true));
    }

    public function bgetNodeNetwork(&$network,
                                   $id,
                                   $username,
                                   $maxLevel,
                                   $currentLevel = 1,
                                   $parent = 0)
    {
        // 如果层数大于等于最大层数时，则不执行任何操作
        if ($currentLevel + 1 > $maxLevel) {
            return;
        }

        if ($currentLevel == 1) {
            $user = Db::name('busers')->alias('su')
                ->join('__USER_LEVEL__ nm','su.level = nm.level',"LEFT")
                ->where('su.mobile', $username)
                ->where('su.id', $id)
                ->find();
            $network[] = [
                'key' => 0,
                'mobile' => $username,
                'level_name' => $user['level_name'],
/*                'leftuse' => $user['area1'],
                'rightuse' =>$user['area2'],*/
                //  'myuse'=>$myuse,
                'is_active'=>1,
                'floor' => '第【' . $currentLevel . '】层'
            ];
        }else{
            $user = Db::name('busers')->alias('su')
                ->join('__USER_LEVEL__ nm','su.level = nm.level',"LEFT")
                ->where('su.mobile',$username)
                ->where('su.id',$id)
                ->find();
        }
/*        var_dump($username);
        var_dump($id);
        var_dump($user);*/
        /*        $user1 = Db::name('users')->alias('su')
                    ->join('__USER_LEVEL__ nm','su.level = nm.level',"LEFT")
                    ->where('su.mobile', $username)
                    ->find();
                var_dump($user1);*/
        //var_dump($network);die();
        //var_dump(122);die();
        if($user['left'] !=0){
            $userone = Db::name('busers')->alias('su')
                ->join('__USER_LEVEL__ nm','su.level = nm.level',"LEFT")
                ->where('su.id',$user['left'])
                ->find();
            //  var_dump($userone);
            $list[0] = array('mobile' => $userone['mobile'],
                'level_name' => $userone['level_name'],
                'id' => $userone['id'],
/*                'leftuse' => $userone['area1'],
                'rightuse' => $userone['area2'],*/
                //  'myuse'=>$myuseone
            );
        }else{
            $list[0] = array();
        }

        if($user['center']!=0){
            $usertwo =Db::name('busers')->alias('su')
                ->join('__USER_LEVEL__ nm','su.level = nm.level',"LEFT")
                ->where('su.id',$user['center'])
                ->find();

            $list[1] = array('mobile' => $usertwo['mobile'],
                'level_name' => $usertwo['level_name'],
                'id' => $usertwo['id'],
/*                'leftuse' => $usertwo['area1'],
                'rightuse' =>$usertwo['area2'],*/
            );
        }else{
            $list[1] = array();
        }
        if($user['right']!=0){
            $userthree =Db::name('busers')->alias('su')
                ->join('__USER_LEVEL__ nm','su.level = nm.level',"LEFT")
                ->where('su.id',$user['right'])
                ->find();

            $list[2] = array('mobile' => $userthree['mobile'],
                'level_name' => $userthree['level_name'],
                'id' => $userthree['id'],
/*                'leftuse' => $usertwo['area1'],
                'rightuse' =>$usertwo['area2'],*/
            );
        }else{
            $list[2] = array();
        }
        // $list[2] = array();
        // 不存在推荐会员则不执行任何操作
        if (empty($list[0])&&empty($list[1])&&empty($list[2])) {
//			var_dump($network);die();
            return;
        }

        // 遍历所有下线，获取数据
        foreach ($list as $item) {
            $key = count($network);

            $network[] = [
                'key' => $key,
                'parent' => $parent,
                'mobile' => $item['mobile'],
                'level_name' => $item['level_name'],
                'leftuse' => $item['leftuse'],
                'rightuse' => $item['rightuse'],
                'id' => $item['id'],
                'is_active'=>1,
                'floor' => '第【' . ($currentLevel + 1) . '】层'
            ];
/*             var_dump($network);*/
            $this->bgetNodeNetwork($network,$item['id'],$item['mobile'], $maxLevel, ++$currentLevel, $key);

            --$currentLevel;
        }
    }
}