<?php
namespace app\admin\controller;
use think\Db;
use \tp5er\Backup;
class Database extends Common
{
    protected $db = '', $datadir;
    function initialize(){
        parent::initialize();
        $this->config=array(
            'path'     => './Data/',//数据库备份路径
            'part'     => 20971520,//数据库备份卷大小
            'compress' => 0,//数据库备份文件是否启用压缩 0不压缩 1 压缩
            'level'    => 9 //数据库备份文件压缩级别 1普通 4 一般  9最高
        );
        $this->db = new Backup($this->config);
    }
    public function database(){
        if(request()->isPost()){
            $list = $this->db->dataList();
            $total = 0;
            foreach ($list as $k => $v) {
                $list[$k]['size'] = format_bytes($v['data_length']);
                $total += $v['data_length'];
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list,'total'=>format_bytes($total),'tableNum'=>count($list),'rel'=>1];
        }
        return view();
    }
    //优化
    public function optimize() {
        $tables = input('tables/a');
        if (empty($tables)) {
            return ['code'=>0,'msg'=>'请选择要优化的表！'];
        }
        if($this->db->optimize($tables)){
            return ['code'=>1,'msg'=>'数据表优化成功！'];
        }else{
            return ['code'=>0,'msg'=>'数据表优化出错请重试！'];
        }
    }
    //修复
    public function repair() {
        $tables = input('tables/a');
        if (empty($tables)) {
            return ['code'=>0,'msg'=>'请选择要修复的表！'];
        }
        if($this->db->repair($tables)){
            return ['code'=>1,'msg'=>'数据表修复成功！'];
        }else{
            return ['code'=>0,'msg'=>'数据表修复出错请重试！'];
        }
    }
    //备份
    public function backup(){
        $tables = input('post.tables/a');
        if (!empty($tables)) {
            foreach ($tables as $table) {
                $this->db->setFile()->backup($table, 0);
            }
            return ['code'=>1,'msg'=>'备份成功！'];
        } else {
            return ['code'=>0,'msg'=>'请选择要备份的表！'];
        }
    }
    //备份列表
    public function restore(){
        if(request()->isPost()){
            $list =  $this->db->fileList();
            return ['code'=>0,'msg'=>'获取成功!','data'=>$list,'rel'=>1];
        }
        return view();
    }
    //执行还原数据库操作
    public function import($time) {
        $list  = $this->db->getFile('timeverif',$time);
        $this->db->setFile($list)->import(1);
        return ['code'=>1,'msg'=>'还原成功！'];
    }

    //下载
    public function downFile($time) {
        $this->db->downloadFile($time);
    }
    //删除sql文件
    public function delSqlFiles() {
        $time = input('post.time');
        if($this->db->delFile($time)){
            return ['code'=>1,'msg'=>"备份文件删除成功！"];
        }else{
            return ['code'=>0,'msg'=>"备份文件删除失败，请检查权限！"];
        }
    }
    public  function  delete(){
        if(request()->isPost()) {
            $password = input('password');
            $admin = db('admin')->where('admin_id', 1)->find();
            if ($admin['pwd'] == md5($password)) {
                db('tixian')->where('id', 'gt', 0)->delete();
                db('money_log')->where('id', 'gt', 0)->delete();
                db('baodan')->where('id', 'gt', 0)->delete();
                db('busers')->where('id', 'gt', 0)->delete();
                db('total_achievement')->where('id', 'gt', 0)->delete();
                $data = array(
                    'recount' => 0,
                    'level' => 1,
                    'plevel' => 0,
                    'money_jifen' => 0,
                    'money_baodan' => 0,
                    'achievement' => 0,
                    'server' => 2,
                    'activation' => 1,
                    'is_lock' => 0,
                    'rpath' => ',',
                    'ppath' => ',',
                    'is_b' => 0,
                    'uid' => 1,
                    'area1' => 0,
                    'area2' => 0,
                    'narea1' => 0,
                    'narea2' => 0,
                    'leftid' => 0,
                    'rightid' => 0,
                );
                db('users')->where('id', 1)->update($data);
                db('users')->where('id', 'gt', 1)->delete();
                return array('code'=>1,'msg'=>'清除成功');
            }else{
                return array('code'=>0,'msg'=>'密码错误！');
            }
        }
        return view();
    }
}